#!/bin/bash

# any future command that fails will exit the script
set -e

# Delete the old repo
rm -rf /home/ubuntu/chat-server

# clone the repo again
git clone git@gitlab.com:SierikovRoman/chat-server.git

#source the nvm file. In an non
#If you are not using nvm, add the actual path like
# PATH=/home/ubuntu/node/bin:$PATH
source /home/ubuntu/.nvm/nvm.sh

#go to our folder
cd /home/ubuntu/chat-server

# fix
pm2 delete chat-server || :

#install npm packages
echo "Running npm install"
npm install

# run gulp 
echo "Running gulp"
gulp build

#start server
echo "Running server"
pm2 start dist/index.js --name "chat-server"