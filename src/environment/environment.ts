export const environment = {
    mongo_url: 'mongodb+srv://admin:admin@chat-cluster-xsour.mongodb.net/chat?retryWrites=true&w=majority',
    jwt_secret: 'secret',
    saltRounds: 10,
    salt: '$2a$10$X2NT0anZTVcRzEtBkAkhA.'
}