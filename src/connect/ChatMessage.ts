import { Schema, Document } from 'mongoose';
import * as mongoose from 'mongoose';

export interface IUChatMessage extends Document {
    chat_id: string,
    message_id: string,
}

const chatMessageSchema: Schema = new Schema ({
    chat_id_id: {
        type: String,
        required: false
    },
    message_id: {
        type: String,
        required: false
    }
})

export default mongoose.model<IUChatMessage>('ChatMessages', chatMessageSchema);