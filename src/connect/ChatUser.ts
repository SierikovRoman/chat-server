import { Schema, Document } from 'mongoose';
import * as mongoose from 'mongoose';

export interface IUChatUser extends Document {
    chat_id?: string,
    user_id?: string,
}

const chatUserSchema: Schema = new Schema ({
    chat_id: {
        type: String,
        required: false
    },
    user_id: {
        type: String,
        required: false
    }
})

export default mongoose.model<IUChatUser>('Chat_User', chatUserSchema);