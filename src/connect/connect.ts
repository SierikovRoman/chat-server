
import * as mongoose from 'mongoose';
import { environment } from '../environment/environment';
import { ConnectType } from '../model/connect';

export class Connect {

    private connect: ConnectType = {
        status: '',
        moongoose: {}
    };
    
    public connectToMongo(): Promise<ConnectType> {
        mongoose.set('useCreateIndex', true);
        return new Promise( (resolve, reject) => {
            mongoose.connect(environment.mongo_url , {useNewUrlParser: true}, (err) => {
                if (err) {
                    this.connect.status = 'Connection error!';
                    reject(this.connect);
                } 
            }).then(
                res => {
                    this.connect.status = 'Successfully connected!';
                    this.connect.moongoose = res;
                    resolve(this.connect);
                }
            )
        })
    }
    
}