import { Schema, Document } from 'mongoose';
import * as mongoose from 'mongoose';

export interface IUMessage extends Document {
    user_id: string,
    message: string,
    dt_created: string
}

const messageSchema: Schema = new Schema ({
    user_id: {
        type: String,
        required: false
    },
    message: {
        type: String,
        required: false
    },
    dt_created: {
        type: String,
        required: false
    }
})

export default mongoose.model<IUMessage>('Messages', messageSchema);