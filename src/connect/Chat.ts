import { Schema, Document } from 'mongoose';
import * as mongoose from 'mongoose';

export interface IUChat extends Document {
    type?: string,
    name?: string,
    dt_created?: string,
    avatar?: string,
}

const chatSchema: Schema = new Schema ({
    type: {
        type: String,
        required: false
    },
    name: {
        type: String,
        required: false
    },
    dt_created: {
        type: String,
        required: false
    },
    avatar: {
        type: String,
        required: false
    }
})

export default mongoose.model<IUChat>('Chat', chatSchema);