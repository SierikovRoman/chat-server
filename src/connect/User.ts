import { Schema, Document } from 'mongoose';
import * as mongoose from 'mongoose';

export interface IUser extends Document {
    username?: string,
    name?: string,
    surname?: string,
    avatar?:  string,
    contacts?: Array<string>,
    password?: string
}

const userSchema: Schema = new Schema ({
    username: {
        type: String,
        unique: true,
        required: true
    },
    name: {
        type: String,
        required: false
    },
    surname: {
        type: String,
        required: false
    },
    avatar: {
        type: String,
        required: false
    },
    contacts: {
        type: Array,
        required: false
    },
    password: {
        type: String,
        required: true
    }
})

export default mongoose.model<IUser>('Users', userSchema);