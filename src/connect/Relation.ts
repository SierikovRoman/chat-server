import { Schema, Document } from 'mongoose';
import * as mongoose from 'mongoose';

export interface IURelation extends Document {
    owner_id: string,
    contact_id: string
}

const relationSchema: Schema = new Schema ({
    owner_id: {
        type: String,
        required: false
    },
    contact_id: {
        type: String,
        required: false
    },
})

export default mongoose.model<IURelation>('Relation', relationSchema);