import { createServer, Server } from 'http';
import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as socketIo from 'socket.io';
import * as cors from 'cors';
import { Message } from './model';
import { environment } from './environment/environment';
import * as jwt from 'jsonwebtoken';

/* Connection to Mongo & creating Shemas */
import { Connect } from './connect/connect';
import { User } from './operarions/user/user';
import { Contact } from './operarions/contact/contact';
import { Chat } from './operarions/chat/chat';
import { Middleware } from './middleware/security';

export class ChatServer {
    public static readonly PORT:number = 3000;
    private app: express.Application;
    private server: Server;
    private io: SocketIO.Server;
    private port: string | number;
    private Profiles: any;
    private token: string;
    private connect: Connect = new Connect;
    private user: User = new User();
    private contact: Contact = new Contact();
    private chat: Chat = new Chat();
    private security: Middleware = new Middleware();

    constructor() {
        this.createApp();
        this.config();
        this.createServer();
        this.sockets();
        this.get();
        this.listen();
        this.connectToMongo();
    }

    private createApp(): void {
        this.app = express();
        this.app.use(cors());
        // this.app.use(express.json());
        this. app.use(express.json({limit: '50mb'}));
        this.app.use(express.urlencoded({limit: '50mb'}));
        // this.app.use(bodyParser.json({limit: '50mb'}));
        // this.app.use(bodyParser.urlencoded({limit: '50mb', extended: true, parameterLimit:50000}));
    }

    private createServer(): void {
        this.server = createServer(this.app);
    }

    private config(): void {
        this.port = process.env.PORT || ChatServer.PORT;
    }

    private sockets(): void {
        this.io = socketIo(this.server);
    }

    private listen(): void {
        this.server.listen(this.port, () => {
            console.log('Running server on port %s', this.port);
        });

        this.io.on('connect', (socket: any) => {
            console.log('Connected client on port %s.', this.port);
            socket.on('message', (m: Message) => {
                console.log('[server](message): %s', JSON.stringify(m));
                this.io.emit('message', m);
            });

            socket.on('disconnect', () => {
                console.log('Client disconnected');
            });
        });
    }

    private connectToMongo() {
        this.connect.connectToMongo().then(
            res => {
                // this.createUser();
            }
        ).catch(
            err => {
                console.log(err);
                
            }
        )
    }

    private get(): void {
        this.app.get('/', (req, res) => {
            res.send(`HEY! IT'S WORKS!`)
        })
        this.app.post('/authorization', (req, res) => {
            const user = req.body;
            this.user.findUser(user)
            .then(
                user => {                   
                    const token = jwt.sign( { id: user._id }, environment.jwt_secret);
                    res.send({code: 200, status: 'success', message: 'User successfully authorizated', token: token});
                }
            )
            .catch(
                err => console.log(err)
            )
        })
        this.app.post('/registration', (req, res) => {
            const user = req.body;
            this.user.createUser(user['username'], user['name'], user['surname'], user['avatar'], user['password'])
            .then(
                token => {
                    res.send({code: 200, status: 'success', message: 'User successfully created', token: token});
                }
            )
            .catch(
                err => console.log(err)
            )
        })
        this.app.get('/user-profile', (req, res) => {
            const token = req.headers.authorization;
            this.security.securityMiddleware(token)
            .then(
                user => {
                    if (user !== null) {
                        res.send({code: 200, status: 'success', message: 'User successfully finded', user: user});
                    } else {
                        res.send({code: 400, status: 'success', message: "User not found", user: null});
                    }
                }
            )
            .catch(
                err => console.log(err)
            )
        })
        // this.app.get('/chat-list', (req, res) => {
        //     const token = req.headers.authorization;
        //     this.security.securityMiddleware(token)
        //     .then(
        //         user => {
        //             if (user !== null) {
        //                 // res.send({code: 200, status: 'success', message: 'User successfully finded', user: user});
        //                 this.chat.
        //             } else {
        //                 res.send({code: 400, status: 'success', message: "User not found", user: null});
        //             }
        //         }
        //     )
        //     .catch(
        //         err => console.log(err)
        //     )
        // })
        this.app.post('/create-contact', (req, res) => {
            const token = req.headers.authorization;
            const contact_id = req.body.contact_id;
            this.security.securityMiddleware(token)
            .then(
                user => {
                    if (user !== null) {
                        this.contact.createContact(user['_id'], contact_id)
                        .then(
                            message => {
                                res.send({code: 200, status: 'success', message: message});
                            }
                        )
                    } else {
                        res.send({code: 400, status: 'success', message: "User not found", user: null});
                    }
                }
            )
            .catch(
                err => console.log(err)
            )
        })
        this.app.get('/contact-list', (req, res) => {
            const token = req.headers.authorization;
            this.security.securityMiddleware(token)
            .then(
                user => {
                    this.contact.getUserContacts(user['_id'])
                    .then(
                        contacts => {
                            if (contacts.length !== 0) {
                                res.send({code: 200, status: 'success', message: 'Contact list', contacts: contacts});
                            } else {
                                res.send({code: 200, status: 'success', message: 'Contact list', contacts: null});
                            }
                        }
                    )
                }
            )
            .catch(
                err => console.log(err)
            )
        })
        this.app.post('/user', (req, res) => {
            const token = req.headers.authorization;
            const user_id = req.body.user_id;
            this.security.securityMiddleware(token)
            .then(
                user => {
                    this.user.findUserById({id: user_id})
                    .then(
                        user => {
                            if (user !== null) {
                                res.send({code: 200, status: 'success', message: 'User successfully finded', user: user});
                            } else {
                                res.send({code: 400, status: 'failed', message: 'User not found', user: null});
                            }
                        }
                    )
                }
            )
            .catch(
                err => console.log(err)
            )
        })
        this.app.post('/create-chat', (req, res) => {
            const token = req.headers.authorization;
            this.security.securityMiddleware(token)
            .then(
                user => {
                    this.chat.createChat(req.body, user._id)
                    .then(
                        chat => {
                            if (chat !== null) {
                                res.send({code: 200, status: 'success', message: 'Chat successfully created', chat: chat});
                            } else {
                                res.send({code: 400, status: 'failed', message: 'Chat not found', chat: null});
                            }
                        }
                    )
                }
            )
            .catch(
                err => console.log(err)
            )
        })
        this.app.get('/chat-list', (req, res) => {
            const token = req.headers.authorization;
            this.security.securityMiddleware(token)
            .then(
                user => {
                    this.chat.getChatList(user._id)
                    .then(
                        chat_list => {
                            if (chat_list !== null) {
                                res.send({code: 200, status: 'success', message: 'Chat list successfully finded', chat_list: chat_list});
                            } else {
                                res.send({code: 400, status: 'failed', message: 'Chat list empty', chat: null});
                            }
                        }
                    )
                }
            )
            .catch(
                err => console.log(err)
            )
        })
    }

    // public appendFile() {
    //     fs.appendFile('./log.txt', 'A request was received\n', (err) => {
    //         if(err) {
    //             return console.log(err);
    //         }
        
    //         console.log("The file was saved!");
    //     });
    // }

    public getApp(): express.Application {
        return this.app;
    }
}