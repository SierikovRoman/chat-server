export interface ModelType {
    name: string,
    schema: object
}