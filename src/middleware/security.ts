import * as jwt from 'jsonwebtoken';
import { environment } from '../environment/environment';
import { IUser } from '../connect/User';
import { User } from '../operarions/user/user';

export class Middleware {

    private user: User = new User();

    public securityMiddleware(token: string): Promise<IUser> {
        const user = jwt.verify(token, environment.jwt_secret);  
        
        return new Promise((resolve, reject) => {
        this.user.findUserById(user)
            .then(
                user => {
                    if (user !== null) {
                        resolve(user)
                    }
                    else {
                        resolve(null)
                    }
                }
            )
            .catch(
                err => reject(err)
            )
        });
    }

}