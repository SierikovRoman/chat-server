import { environment } from '../../environment/environment';
import UserModel, { IUser } from '../../connect/User';
import RelationModel, { IURelation } from '../../connect/Relation';


export class Contact {

    public createContact(user_id: string, contact_id: string): Promise<string> {

        return new Promise( (resolve, reject) => {
            const newContact = new RelationModel ({
                owner_id: user_id,
                contact_id: contact_id
            });
            newContact.save()
            .then(
                _ => {
                    resolve('User success added to your contact list');
                }
            )
            .catch(
                err => console.log(err)
            )
        })
    }

    public getUserContacts(user_id: string): Promise<Array<IUser>> {        

        return new Promise( (resolve, reject) => {
            
            RelationModel.find({'owner_id': user_id}, 'contact_id')
            .then(
                contacts => {
                    let contact_list: Array<string> = [];
                    for(let i=0; i < contacts.length; i++) {
                        contact_list.push(contacts[i].contact_id)
                    }
                    UserModel.find({ '_id': { $in: contact_list } })
                    .then(
                        res => {
                            // console.log(res);   
                            resolve(res); 
                        }
                    )   
                }
            )
        })
    }

}