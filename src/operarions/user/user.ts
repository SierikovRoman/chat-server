import * as bcrypt from 'bcryptjs';
import * as jwt from 'jsonwebtoken';
import { environment } from '../../environment/environment';
import UserModel, { IUser } from '../../connect/User';


export class User {

    public createUser(usename: string, name: string, surname: string, avatar: string, password: string): Promise<string> {

        return new Promise((resolve, reject) => {
            const hash = bcrypt.hashSync(password, environment.salt);
            const newUser = new UserModel ({
                username: usename,
                name: name,
                surname: surname,
                avatar: avatar,
                password: hash,
            });
            newUser.save()
            .then(user => {
                const token = jwt.sign( { id: user._id }, environment.jwt_secret);
                resolve(token);
            })
            .catch(err => {
                if (err.code === 11000)
                reject('Username is already exist!');
            });

        })
    }

    public findUser(user: any): Promise<IUser> {  

        return new Promise((resolve, reject) => {
            
            UserModel.findOne({username: user.username})
            .then(f_user => {
                if (f_user !== null) {
                    bcrypt.compare(user.password, f_user.password).then(
                        res => {
                            if (res) {
                                // console.log('User: ', f_user);
                                resolve(f_user);
                            } else {
                                // password was incorect
                                // console.log('Faild: ', f_user);
                                resolve(null);
                            }
                        },
                        err => {
                            reject(err)
                        }
                    );
                } else {
                    // user wasn't found
                    resolve(null);
                }
            })
            .catch(err => {
                reject(err);
            });

        })
    }

    public findUserById(user: any): Promise<IUser> {        
        return new Promise((resolve, reject) => {
            UserModel.findById(user.id)
            .then(user => {
                if (user !== null) {
                    resolve(user);
                } else {
                    resolve(null);
                }
            })
            .catch(err => {
                reject(err);
            });

        })
    }

    public removeUser() {

    }

    public updateUser() {

    }

}