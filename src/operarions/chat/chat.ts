import { environment } from '../../environment/environment';
import ChatModel, { IUChat } from '../../connect/Chat';
import ChatUserModel, { IUChatUser } from '../../connect/ChatUser';
import UserModal, { IUser } from '../../connect/User';


export class Chat {

    public createChat(chat: any, user_id: string): Promise<String> {

        return new Promise((resolve, reject) => {

            UserModal.findById(chat.contact_id)
            .then(
                user => {
                    if (user !== null) {
                        const newChat = new ChatModel ({
                            type: chat.type,
                            name: `${ user.name } ${ user.surname }`,
                            dt_created: chat.dt_created,
                            avatar: user.avatar
                        });
                        newChat.save()
                        .then(created_chat => {
                            const chat_id = created_chat._id;
                            resolve('success');
                            this.createChatUsers(chat_id, user_id, chat.contact_id);
                        })
                        .catch(err => {
                            if (err.code === 11000)
                            reject('Chat is already exist!');
                        });
                    }
                }
            )

        })

    }

    public createChatUsers(chat_id: string, user_id: string, contact_id: string): Promise<String> {

        return new Promise((resolve, reject) => {
            const newChatUser = new ChatUserModel ({
                chat_id: chat_id,
                user_id: user_id
            });
            newChatUser.save()
            .then(chat_users => {
                const newChatUser = new ChatUserModel ({
                    chat_id: chat_users.chat_id,
                    user_id: contact_id
                });
                newChatUser.save()
                .then(
                    res => {
                        resolve('success');
                    }
                )
            })
            .catch(err => {
                if (err.code === 11000)
                reject('Chat is already exist!');
            });

        })

    }

    public getChatList(user_id: string): Promise<Array<IUChat>> {

        return new Promise((resolve, reject) => {
            ChatUserModel.find({ 'user_id':  user_id})
            .then(chats => {
                let chat_list: Array<string> = [];
                for(let i=0; i < chats.length; i++) {
                    chat_list.push(chats[i].chat_id)
                }
                // console.log(chat_list);
                
                ChatModel.find({ '_id': { $in: chat_list } })
                .then(
                    chat_list => {
                        console.log(chat_list);
                        resolve(chat_list);
                    }
                )
                
            })
            .catch(err => {
                if (err.code === 11000)
                reject('Chat is already exist!');
            });

        })

    }

    public getChatById(chat_id: number) {

        // MessageModel.findById(chat_id)
        // .then(
        //     chat => {
        //         console.log(chat);
        //     }
        // )
        // .catch(
        //     err => console.log(err)
        // )

    }

}